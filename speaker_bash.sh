#!/bin/bash

tmp=1;
loaded=0;
power=0;
while [ $tmp -eq 1 ];
do
    date;
    mainMenu="
==========================
        MAIN MENU
==========================
1. Load module speaker
2. Unload module speaker
3. Change speaker volume
4. Check speaker volume
5. Disable speaker
6. Enable speaker
7. Exit
";
    echo "$mainMenu";
    echo -n "choose 1-7: ";
    read num;
	
    if [ $num -eq "1" ];
        then
        modprobe snd_hda_intel
	    loaded=1;
	    echo "snd_hda_intel successfully loaded"

    elif [ $num -eq "2" ];
        then
        modprobe -r snd_hda_intel
	    loaded=0;
	    echo "snd_hda_intel successfully unloaded"

    elif [ $num -eq "3" ];
        then
        if [ $loaded -eq "0" ];
            then
            printf "Speaker Volume (use %%): "
            read newvolume
            amixer set 'Master' $newvolume
            echo "Speaker volume succesfully changed"
            loaded=1;

	    elif [ $loaded -eq "1" ];
            then
            printf "Speaker Volume (use %%): "
            read newvolume
            amixer set 'Master' $newvolume
            echo "Speaker volume succesfully changed"
            loaded=1;
        fi
        
    elif [ $num -eq "4" ];
        then
	    amixer get Master | awk '$0~/%/{print $4}' | tr -d '[]'

    elif [ $num -eq "5" ];
        then
	    if [ $power -eq "0" ];
            then
            echo "Speaker already disabled"
            

	    elif [ $power -eq "1" ];
            then
            mocp -x &> /dev/null
            modprobe -r snd_hda_intel
            modprobe snd_hda_intel enable=N
            echo "Speaker has been disabled"
            power=0;
        fi

    elif [ $num -eq "6" ];
        then
	    if [ $power -eq "1" ];
            then
            echo "Speaker already enabled"
            

	    elif [ $power -eq "0" ];
            then
            mocp -x &> /dev/null
            modprobe -r snd_hda_intel
            modprobe snd_hda_intel enable=Y
            echo "Speaker has been enabled"
            power=1;
        fi

    elif [ $num -eq "7" ];
        then
            echo "Bye Bye...."
            tmp=0;
    fi
        echo -n "Press [Enter] key to continue...";
        read blank
    done