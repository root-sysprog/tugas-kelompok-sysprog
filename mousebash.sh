#!/bin/bash

printf "New Device Accel Constant Deceleration: "
read NEW_ACCEL
for i in $(xinput --list | awk -F '\t' '{ print $2 }' | awk -F '=' '{ print $2 }'); do
	OUTPUT=$(xinput --list-props $i | awk -F '\t' '/Device Accel Constant Deceleration/{ print $2 }' | awk '{ print $NF }' | tr -d '():')
	if [ "$OUTPUT" != "" ]; then
		echo "Setting driver $i"
		echo "Param $OUTPUT"
		echo "To $NEW_ACCEL"
		xinput set-prop $i $OUTPUT $NEW_ACCEL
	fi
done
