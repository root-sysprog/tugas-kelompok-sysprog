#!/bin/bash

for i in $(xinput --list | awk -F '\t' '{ print $2 }' | awk -F '=' '{ print $2 }'); do
	OUTPUT=$(xinput --list-props $i | awk -F '\t' '/Device Accel Constant Deceleration/{ print $2 " " $3 }')
	if [ "$OUTPUT" != "" ]; then
		echo "Device $i"
		echo "$OUTPUT"
	fi
done
