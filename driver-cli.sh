#!/bin/bash

programState=0

while [[ programState -ne 5 ]]; do
  echo "==========================="
  echo "         MAIN MENU         "
  echo "==========================="
  echo "1. Mouse Info"
  echo "2. Change Mouse Speed"
  echo "3. Speaker Info"
  echo "4. Change Speaker Volume"
  echo "5. Exit"
  printf "choose 1-5: "
  read programState
  echo ""

  if [[ programState -eq 1 ]]; then
    echo "-----------------------------------------------------"
    echo "                     MOUSE INFO                      "
    echo "-----------------------------------------------------"
    bash mouseinfo.sh
  elif [[ programState -eq 2 ]]; then
    echo "-----------------------------------------------------"
    echo "                 CHANGE MOUSE SPEED                  "
    echo "-----------------------------------------------------"
    bash mousebash.sh
  elif [[ programState -eq 3 ]]; then
    echo "-----------------------------------------------------"
    echo "                    SPEAKER INFO                     "
    echo "-----------------------------------------------------"
    bash speakerinfo.sh
  elif [[ programState -eq 4 ]]; then
    echo "-----------------------------------------------------"
    echo "                 CHANGE SPEAKER VOLUME               "
    echo "-----------------------------------------------------"
    bash speakerbash.sh
  elif [[ programState -eq 5 ]]; then
    echo "Bye Bye...."
  else
    echo "Invalid choice"
    echo ""
    continue
  fi

  echo ""
  printf "Press [Enter] key to continue..."
  read temp
  echo ""
done
