# ROOT

## Kelompok SYSTEM PROGRAMMING KELAS A (2020)

### How to run the program

*driver-cli.sh*
- bash driver-cli.sh

*mouseinfo.sh*
- bash mouseinfo.sh

*mousebash.sh*
- bash mousebash.sh

*speakerinfo.sh*
- bash speakerinfo.sh

*speakerbash.sh*
- bash speakerbash.sh

*speaker_bash.sh*
- sudo su
- ./speaker_bash.sh

untuk menjalankan music playernya, disini kami menggunakan MoC music player linux:
- apt-get update
- apt-get install moc
- mocp

### Anggota Kelompok:

- Muhammad Iqbal Wijonarko - 1806186736
- Ferdi Salim Sungkar - 1806173576
- Firdaus Alghifari - 1806133780
